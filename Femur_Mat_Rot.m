% Author     :   M. Fonseca
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/mis_markers_misplacement
% Reference  :   "Impact of knee marker misplacement on gait kinematics of 
%                children with cerebral palsy using the Conventional Gait Model - A sensitivity study" 
%                M. Fonseca, X. Gasparutto, F. Leboeuf, R. Dumas, S. Armand; Plos One
% Date       :   April 2020
% -------------------------------------------------------------------------
% Description:   Define the femoral rotation matrix
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [ Rot_femur_r ] = Femur_Mat_Rot(origin,proximal,lateral,anterior)

%UNTITLED3 Summary of this function goes here
Y = lateral - origin;
Y = Y/sqrt(Y(1)^2+Y(2)^2+Y(3)^2);

X = anterior - origin;
X = X/sqrt(X(1)^2+X(2)^2+X(3)^2);

Z = cross(X,Y);
Z = Z/sqrt(Z(1)^2+Z(2)^2+Z(3)^2);

Rot_femur_r=[X',Y',Z'];
% A12=transpose(A21);

end