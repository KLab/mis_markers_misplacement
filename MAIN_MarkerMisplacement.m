% Author     :   M. Fonseca
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/mis_markers_misplacement
% Reference  :   "Impact of knee marker misplacement on gait kinematics of 
%                children with cerebral palsy using the Conventional Gait Model - A sensitivity study" 
%                M. Fonseca, X. Gasparutto, F. Leboeuf, R. Dumas, S. Armand; Plos One
% Date       :   April 2020
% -------------------------------------------------------------------------
% Description:   Routine to check whether the fusion of motion capture and medical imaging
%                can reduce the marker misplacement errors
% Process    :   1 - User define misplacement
%                2 - Open .c3d files
%                3 - Compute Marker Misplacement and export kinematics
%                4 - Calculate RMSD 
%                5 - Plot RMSD in polar plot
%                6 - Scatter plot
% Dependencies : https://github.com/Biomechanical-ToolKit/BTKCore
%                https://github.com/pyCGM2/pyCGM2
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------
close all
clear all
clc

%% 1. Create virtual marker (Define before computation)
% MAR1: marker to misplace.
% SEGMENT: segment where the marker is used to define the LCS.
% Er: error magnitudes in mm.
% Error_dir: 'AP_ML' antero-posteior + medial lateral (e.g. LKNE, LANK);'AP_DP' antero-posteior + proximal distal; 'ML_PD' medial lateral + proximal distal (e.g Lasi);
% seg_origin: segment origin
MARK1 = ('LKNE');
Er = [0, 5, 10, 15, 20, 30];
Error_dir = 'AP_ML';

SEGMENT.name = ('LFEMUR');
SEGMENT.origin = ('LHJC');
SEGMENT.proximal = [SEGMENT.name, '_Z'];
SEGMENT.lateral  = [SEGMENT.name, '_Y'];
SEGMENT.anterior = [SEGMENT.name, '_X'];
b=1;
angle = [0; 45; 90; 135; 180; 225; 270; 315];

%% 2. Open .c3d files
% 2.1 - Path to original data files (containing 1 static and 1 dynamic file)
original_datapath = 'D:\GITLAB\GAIT DATA\Marker_MIsplacement_Original_DATA\TD_group\';
% 2.2 - Path to copy and compute simulation over the data 
data_path  = 'D:\GITLAB\GAIT DATA\Marker_Misplacement_LKNE_DATA\TD_group\';
% 2.3 - Ask user to select data
patients   = dir([data_path,'*.c3d']);
[C3D_filenames, C3D_path, FilterIndex]=uigetfile({'*.C3D'},'Please select C3D files',[original_datapath '/'],'MultiSelect','on');
cd(original_datapath)

% Move and rename files
for i=1:length(C3D_filenames)
    copyfile(char(C3D_filenames{i}),data_path);
    C3D_filenames{i} = char(C3D_filenames{i});
end

%% 3. Compute Marker Misplacement + Kinematics
[Angles, MM, counter, Error] = MIS_Computation(C3D_filenames, MARK1, SEGMENT, Er, Error_dir, angle, b, data_path);
save Angles

%% 4. Calculate RMSD 
% compute the results and export RMSD, std and max and mean(over patients)
[T_RMSD, T_std, T_max, T_m_RMSD, T_m_max, m_RMSD, m_RMSD_pMagn, T_m_RMSD_pMagn, R]  = MIS_Res_RMSD(Angles, Error, Er, MM, counter); 

[RR, Table1] = MIS_table_RMSD(Angles, Error, Er, MM, counter, data_path, C3D_filenames); 
writetable(Table1, 'Table Results RMSD.xls');

%% 5. Polar Plot
Lable = {'     Ant', 'Ant + Dist', 'Dist', 'Post + Dist', 'Post', 'Post + Prox', 'Prox', 'Ant + Prox'};
LineColor = {'b', 'c', 'g', 'm', 'r'};
LineStyle = {'no', ':'};
LevelNum = 5;
maximo = 17;
figure(1)
MIS_PlotPolar(m_RMSD_pMagn, Error)

%% 6.  Scatter Plot - correlation between RMSD and leg length
% 6.1 - Export anthropometric data
anthro = MIS_anthropometric_data(C3D_filenames, data_path);               
Leg_length = zeros(1, length(anthro.subject));
Knee_Width = zeros(1, length(anthro.subject));
 for i = 1:length(anthro.subject)
     Leg_length(i) = str2double(anthro.subject(i).Left_LegLength_mm);
     Knee_Width(i) = str2double(anthro.subject(i).Left_KneeWidth_mm);
 end
 
 % 6.2 - Case 1: Regression for each patient. Plot all regressions. Table with mean values from the overral regressions
 figure(2)
 hold on
 for i = 1:10
    [correl_LL, C, Table2] = MIS_Correlation(Leg_length(i), R, Er, 6, 30, 'Leg length', i);   % correl = correlation between  error and leg_length normalized; norm_errors:
 end
 
% 6.3 - Case 2: Combine all values and perform a general regression.
figure (3) 
direction = 'Ant'; 
x_lim = 6;
y_lim = 30;
[Correl_all, C, Table_Correl] = MIS_Correlation_all(Leg_length, R, Er, x_lim, y_lim, counter, direction)